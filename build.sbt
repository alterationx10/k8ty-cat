import com.typesafe.sbt.packager.docker.Cmd

lazy val akkaHttpVersion = "10.1.4"
lazy val akkaVersion    = "2.5.16"

lazy val k8ty = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "io.evillair",
      scalaVersion    := "2.12.7",
      version := "latest"
    )),
    name := "k8ty-cat",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test,

      "me.atrox.haikunator" % "Haikunator" % "2.0.0",

      "redis.clients" % "jedis" % "3.0.0",

      "com.typesafe.slick" %% "slick" % "3.2.3",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.2.3",
      "com.github.tminglei" %% "slick-pg" % "0.17.0"

    )
  )
  .enablePlugins(SbtTwirl)
  .enablePlugins(JavaAppPackaging)

dockerBaseImage  := "openjdk:8-jre-alpine"
dockerExposedPorts := Seq(9000)
packageName := "alterationx10/k8ty-cat"
dockerRepository := Some("registry.gitlab.com")
// Add in bash to out alpine image
dockerCommands := dockerCommands.value.flatMap{
  case cmd@Cmd("FROM",_) => List(cmd, Cmd("RUN", "apk add --no-cache bash"))
  case other => List(other)
}

