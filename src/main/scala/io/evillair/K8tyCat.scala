package io.evillair

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import io.evillair.routes.{ HaikunatorRoute, HealthRoute, ResourceRoute }

import scala.concurrent.ExecutionContextExecutor

object K8tyCat extends App {

  implicit val system: ActorSystem = ActorSystem("k8ty-cat")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val host: String = "0.0.0.0"
  val port: Int = 9000

  val route = {
    HealthRoute.route ~
      ResourceRoute.route ~
      HaikunatorRoute.route
  }

  val bindingFuture = Http().bindAndHandle(route, host, port)
  println(s"Server online at http://$host:$port")

}
