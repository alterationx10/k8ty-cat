package io.evillair.modules.config

import com.typesafe.config.{ Config, ConfigFactory }

import scala.util.Try

object Configuration {

  implicit class EnhancedConfig(config: Config) {

    def readTry[A](read: Config => A): Try[A] = Try {
      read(config)
    }

    def readOpt[A](read: Config => A): Option[A] = Try {
      Option {
        read(config)
      }
    }.toOption.flatten

    def getStringOpt(path: String): Option[String] = readOpt(_.getString(path))
    def getIntOpt(path: String): Option[Int] = readOpt(_.getInt(path))
    def getBooleanOpt(path: String): Option[Boolean] = readOpt(_.getBoolean(path))

  }

  lazy val config: Config = ConfigFactory.load()

}
