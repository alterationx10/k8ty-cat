package io.evillair.modules.redis

import com.typesafe.config.Config
import io.evillair.modules.config.Configuration
import redis.clients.jedis.{ Jedis, JedisPool, JedisPoolConfig }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

object RedisClient {

  lazy val redisConfig: Config = Configuration.config.getConfig("io.evillair.modules.redis")

}

trait RedisClient {

  import RedisClient._

  val host: String
  val password: Option[String]

  lazy val jedisPool: JedisPool = password match {
    case Some(p) => new JedisPool(jedisPoolConfig, host, port, timeOut, p)
    case None => new JedisPool(jedisPoolConfig, host, port, timeOut)
  }

  val port: Int = redisConfig.getInt("port")
  val timeOut: Int = redisConfig.getInt("timeOut")
  val minIdleConnections: Int = redisConfig.getInt("minIdleConnections")
  val maxIdleConnections: Int = redisConfig.getInt("maxIdleConnections")
  val maxConnections: Int = redisConfig.getInt("maxConnections")
  val testConnectionOnReturn: Boolean = redisConfig.getBoolean("testConnectionOnReturn")
  val testConnectionWhileIdle: Boolean = redisConfig.getBoolean("testConnectionWhileIdle")
  val softMinEvictableIdleTimeMillis: Int = redisConfig.getInt("softMinEvictableIdleTimeMillis")

  /**
   * The redis connection pool. Default settings from reference.conf are:
   *   25 minimum connections in the pool.
   *   100 max connections in the pool.
   *   Unlimited connections on burst, if needed.
   *   Test connection on return to the pool.
   *   Test connections while idle.
   *   Evict members of the connection pool that have been idle for 5 minutes.
   */
  val jedisPoolConfig: JedisPoolConfig = {
    val jpc: JedisPoolConfig = new JedisPoolConfig()
    jpc.setMinIdle(minIdleConnections)
    jpc.setMaxIdle(maxIdleConnections)
    jpc.setMaxTotal(maxConnections)
    jpc.setTestOnReturn(testConnectionOnReturn)
    jpc.setTestWhileIdle(testConnectionWhileIdle)
    jpc.setSoftMinEvictableIdleTimeMillis(softMinEvictableIdleTimeMillis)
    jpc
  }

  def redisTryAction[T](thunk: Jedis => T): Try[Option[T]] = Try {
    val jedis: Jedis = jedisPool.getResource
    val result: T = thunk(jedis)
    jedis.close()
    Option(result)
  }

  def redisTryActionAsync[T](thunk: Jedis => T): Future[Try[Option[T]]] = {
    Future {
      redisTryAction(thunk)
    }
  }

  def redisAction[T](thunk: Jedis => T): Option[T] = redisTryAction(thunk).toOption.flatten

  def redisActionAsync[T](thunk: Jedis => T): Future[Option[T]] = {
    Future {
      redisAction(thunk)
    }
  }

  sys.addShutdownHook {
    jedisPool.close()
  }

}
