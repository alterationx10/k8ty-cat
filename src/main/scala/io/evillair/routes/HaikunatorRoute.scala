package io.evillair.routes

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import me.atrox.haikunator.Haikunator
import akka.http.scaladsl.model.StatusCodes._

/**
 * A route to return a sweet haiku-like project name
 */
object HaikunatorRoute extends K8tyRoute {

  val haikunator: Haikunator = new Haikunator()

  override val slug: String = "haikunate"

  override val route: Route = ignoreTrailingSlash {

    pathPrefix(slug) {

      pathEndOrSingleSlash {
        get {
          complete {
            HttpResponse(OK, entity = haikunator.setTokenHex(true).haikunate())
          }
        }
      }

    }
  }

}
