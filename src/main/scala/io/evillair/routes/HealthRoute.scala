package io.evillair.routes

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.model.StatusCodes._

/**
 * A route to return 200 OK for literally anything at /_health and beyond
 */
object HealthRoute extends K8tyRoute {
  override val slug: String = "_health"
  override val route: Route = ignoreTrailingSlash {
    pathPrefix(slug) {
      get {
        complete {
          HttpResponse(OK)
        }
      }
    }
  }
}
