package io.evillair.routes

import akka.http.scaladsl.server.Route

/**
 * A trait for all of K8ty's Routes to extend
 */
trait K8tyRoute {

  val slug: String
  val route: Route

}
