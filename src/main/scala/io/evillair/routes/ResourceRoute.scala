package io.evillair.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

/**
 * A route to return bundled static resource files (js, css, etc...)
 * located in the folder 'static' in the resources directory
 */
object ResourceRoute extends K8tyRoute {
  override val slug: String = "static"
  override val route: Route = ignoreTrailingSlash {
    pathPrefix(slug) {
      getFromResourceDirectory(slug)
    }
  }
}
