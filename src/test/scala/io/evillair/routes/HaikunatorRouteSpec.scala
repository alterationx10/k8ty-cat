package io.evillair.routes

import org.scalatest.{ Matchers, WordSpec }
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest

class HaikunatorRouteSpec extends WordSpec with Matchers with ScalatestRouteTest {

  val mainSlug: String = s"/${HaikunatorRoute.slug}"
  val trailingSlash: String = s"$mainSlug/"

  "The HaikunatorRoute" should {

    s"return an 200 OK at $mainSlug" in {

      Get(mainSlug) ~> HaikunatorRoute.route ~> check {
        status shouldEqual StatusCodes.OK
        assert(responseAs[String].nonEmpty)
      }

    }

    s"return an 200 OK at $trailingSlash" in {

      Get(trailingSlash) ~> HaikunatorRoute.route ~> check {
        status shouldEqual StatusCodes.OK
        assert(responseAs[String].nonEmpty)
      }

    }

  }

}
