package io.evillair.routes

import org.scalatest.{ Matchers, WordSpec }
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest

import scala.util.Random

class HealthRouteSpec extends WordSpec with Matchers with ScalatestRouteTest {

  val mainSlug: String = s"/${HealthRoute.slug}"
  val trailingSlash: String = s"$mainSlug/"
  val randomPath: String = s"$trailingSlash${Random.alphanumeric.take(5).mkString}"

  "The HealthRoute" should {

    s"return an empty 200 OK at $mainSlug" in {

      Get(mainSlug) ~> HealthRoute.route ~> check {
        status shouldEqual StatusCodes.OK
        assert(responseAs[String].isEmpty)
      }

    }

    s"return an empty 200 OK with a trailing slash at $trailingSlash" in {

      Get(trailingSlash) ~> HealthRoute.route ~> check {
        status shouldEqual StatusCodes.OK
        assert(responseAs[String].isEmpty)
      }

    }

    s"return an empty 200 OK anything beyond $mainSlug => $randomPath" in {

      Get(randomPath) ~> HealthRoute.route ~> check {
        status shouldEqual StatusCodes.OK
        assert(responseAs[String].isEmpty)
      }

    }

  }

}
