package io.evillair.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{ Matchers, WordSpec }

import scala.util.Random

class ResourceRouteSpec extends WordSpec with Matchers with ScalatestRouteTest {

  val mainSlug: String = s"/${ResourceRoute.slug}"
  val existingResource: String = s"$mainSlug/test.txt"
  val randomPath: String = s"$mainSlug/${Random.alphanumeric.take(5).mkString}/none.txt"

  "The ResourceRoute" should {

    s"handle a resource that exists at $existingResource" in {
      Get(existingResource) ~> ResourceRoute.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    s"not handle a resource that don't exist" in {
      Get(randomPath) ~> ResourceRoute.route ~> check {
        handled shouldEqual false
      }
    }

  }

}
